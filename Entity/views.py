# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,get_object_or_404
import json
from .models import CT,ExtraField,Value
from django.views.generic import View
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from models import CT,ExtraField,Value,CT_instances
from django.http import QueryDict 
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.
class Container_type(View):
    def get(self, request):
        container_list = CT.objects.all()
        result = []
        for container in container_list:
            res = {}
            res['id'] = container.cont_id
            res['name'] = container.cont_name 
            res['created_by'] = container.created_on
            res['updated_by'] = container.updated_on
            result.append(res)
        return JsonResponse({"containers": result}, status=200)
    def post(self, request):
        res = request.POST 
        ct_id = res['cont_id']
        name = res['cont_name']
        obj =  CT(cont_name=name, cont_id = ct_id )
        obj.save()
        return JsonResponse(({
            'new container ID':ct_id,
            'New Container Name' : name
        }), 
        status = 201, content_type = 'application/json')

class ExtraFieldListView(View):
    def get(self, request, ct_id):         
        containers = ExtraField.objects.filter(container_type = ct_id)
        print containers
         
        data = serializers.serialize('json', containers)
        try:    
                    return HttpResponse(data, content_type = 'application/json',status = 200) 
        except ObjectDoesNotExist:
                    return HttpResponse('object does not exist') 
    def post(self, request, ct_id):
        field = ExtraField.objects.filter(container_type_id = ct_id)
        res = request.POST
        name = res['ef_name']
        extra_field_id = res['ef_id']
        fields = res['field_type']
        obj = ExtraField(ef_name = name, ef_id = extra_field_id, container_type_id = ct_id, field_type = fields)
        obj.save()
        return JsonResponse(({
            'Extra Field name' :name,
            'Extra Field Id' :extra_field_id,
            'Extra Field Type' :fields
        }), content_type = 'application/json', status = 201)

class ExtraFields(View):
    def get (self, request):
        ef_list = ExtraField.objects.all()
        result = [ ]
        for ef in ef_list:
            res = {}
            res['id'] = ef.ef_id
            res['name'] = ef.ef_name
            res['ct_type'] = ef.container_type.cont_name
            res['ef_type'] = ef.field_type
            result.append(res)
        return JsonResponse(({
            'extrafields' : result
        }), content_type = 'application/json', status = 200)
class Container_instance(View):
    def get(self, request, ct_id):
        containers = CT_instances.objects.filter(ct_instance = ct_id)
        print containers
        result=[]
        for i in containers:
            field_values = Value.objects.filter(container_id = i.pk)
            data = []
            for field in field_values:
                
                res = {}
                res['ef_id'] = field.ef.ef_id
                res['ef_name'] = field.ef.ef_name
                res['ef_value_id'] = field.pk   
               
                if field.ef.field_type == 'text':
                    res['ef_value'] = field.char_value    
                    data.append(res)
                elif field.ef.field_type == 'number':
                    res['ef_value'] = field.int_value
                    data.append(res)
                else :
                    res['ef_value'] = str(field.date_value)
                    data.append(res)
            if data != []:        
                result.append({
                    'Container_name' :  i.ct_instance.cont_name,
                    'data' : data,
                    'id' : i.pk
                })           
        return JsonResponse(({'containers' : result}),content_type='application/json')

    def post(self,request,ct_id):
        # import pdb; pdb. set_trace()
        ct_instance = request.POST['ct_instance']
        ef_id= request.POST['ef_id']
        ef_name = request.POST['ef_name']
        ef= request.POST['ef_value']
        result = ({
            'ct_instance' :ct_instance,
            'ef_id' : ef_id,
            'ef_name': ef_name,
            'value' : ef 
        })
        cti=CT_instances.objects.filter(ct_instance_id =  ct_id)
        print cti
        obj=get_object_or_404(ExtraField, ef_id = ef_id)
              
        cti_new = CT_instances.objects.create(ct_instance_id =  ct_id) 
        if obj.field_type=='number':
                    data = Value(int_value = ef, ef_id = obj.ef_id, container_id_id = cti_new.pk)
                    data.save()
                    return JsonResponse(({'newmovie':result}), content_type = 'application/json')
        elif obj.field_type == 'text':
                    data = Value(char_value = ef, ef_id = obj.ef_id, container_id_id = cti_new.pk)
                    data.save()
                    return JsonResponse(({'newmovie':result}), content_type = 'application/json')
        else:
                    data=Value(date_value = ef ,ef_id = obj.pk, container_id_id = cti_new.pk)
                    data.save()
                    return JsonResponse(({'newmovie':result}), content_type = 'application/json')

