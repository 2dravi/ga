# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

                
class CT(models.Model):
        cont_id = models.CharField(max_length = 255, primary_key = True)
        cont_name = models.CharField(max_length=255)
        created_on = models.DateTimeField(auto_now_add=True, null = True)
        updated_on = models.DateTimeField(auto_now=True, null = True)
        def  __str__(self):
             return (self.cont_id)       

class CT_instances(models.Model):
        ct_instance = models.ForeignKey(CT)
        created_on = models.DateTimeField(null =True, blank=True)
        updated_on = models.DateTimeField(null = True , blank = True)
        def __unicode__(self):
                return ' %s ' % (self.id) 
 

class ExtraField(models.Model):
        TEXT = 'text'
        NUMBER = 'number'
        DATE = 'date'
        
        TYPE_CHOICES = (
                	(TEXT,'text'),
			(NUMBER,'number'),
                        (DATE,'date')
        )
        ef_id = models.CharField(max_length = 255, primary_key = True)
        ef_name = models.CharField(max_length = 255)
        container_type = models.ForeignKey(CT)
        field_type = models.CharField(max_length=255, choices=TYPE_CHOICES)
        def __unicode__(self):
            return (self.ef_name)


class Value(models.Model):
        ef = models.ForeignKey(ExtraField)
        container_id = models.ForeignKey(CT_instances)
        int_value = models.IntegerField(null= True, blank=True)
        char_value = models.TextField(null= True, blank=True)
        date_value = models.DateField(null=True, blank = True)
        def __unicode__(self):
            if self.int_value == None:  
                return ' %s ' % (self.char_value)
            elif self.char_value == None:
                return '%s' % (self.date_value)
            else:
                return '%s' %(self.int_value)         
  


