# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import CT,ExtraField,Value,CT_instances

# Register your models here.
admin.site.register(CT)
admin.site.register(ExtraField)
admin.site.register(Value)
admin.site.register(CT_instances)