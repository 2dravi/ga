from django.conf.urls import url
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt
from django.views import View
from views import Container_type,ExtraFields,Container_instance, ExtraFieldListView


urlpatterns = [
    url(r'^container_type/$',csrf_exempt(Container_type.as_view())),
    url(r'^extra_fields/$',csrf_exempt(ExtraFields.as_view())),
    url(r'^extra_fields/container_type/(?P<ct_id>\d+)/$', csrf_exempt(ExtraFieldListView.as_view())),
    url(r'^container_instances/container_type/(?P<ct_id>\d+)/$', csrf_exempt(Container_instance.as_view())),
    url(r'^container_instances/container_type/$', csrf_exempt(Container_instance.as_view())),
]